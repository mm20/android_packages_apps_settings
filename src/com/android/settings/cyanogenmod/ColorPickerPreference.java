package com.android.settings.cyanogenmod;

import com.android.settings.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.preference.DialogPreference;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by android on 08.11.15.
 */
public class ColorPickerPreference extends DialogPreference implements AdapterView.OnItemClickListener {

    private static int[] sColors = new int[]{
            Color.parseColor("#F44336"), // Red
            Color.parseColor("#E91E63"), // Pink
            Color.parseColor("#9C27B0"), // Purple
            Color.parseColor("#673AB7"), // Deep Purple
            Color.parseColor("#3F51B5"), // Indigo
            Color.parseColor("#2196F3"), // Blue
            Color.parseColor("#03A9F4"), // Light Blue
            Color.parseColor("#00BCD4"), // Cyan
            Color.parseColor("#009688"), // Teal
            Color.parseColor("#4CAF50"), // Green
            Color.parseColor("#8BC34A"), // Light Green
            Color.parseColor("#CDDC39"), // Lime
            Color.parseColor("#FFEB3B"), // Yellow
            Color.parseColor("#FFC107"), // Amber
            Color.parseColor("#FF9800"), // Orange
            Color.parseColor("#FF5722"), // Deep Orange
            Color.parseColor("#795548"), // Brown
            Color.parseColor("#9E9E9E"), // Grey
            Color.parseColor("#607D8B")  // Blue Grey
    };

    private int mSelectedItem;
    private GridView mGridView;
    private ImageView mIndicator;

    public ColorPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mSelectedItem = Settings.System.getInt(getContext().getContentResolver(), "qs_accent_color",
                6);
        setWidgetLayoutResource(R.layout.preference_widget_color_picker);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        mIndicator = (ImageView) view.findViewById(R.id.colorPickerIndicator);
        mIndicator.setColorFilter(sColors[mSelectedItem], PorterDuff.Mode.MULTIPLY);

    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        return super.onCreateView(parent);
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        super.onPrepareDialogBuilder(builder);

        mGridView = new GridView(getContext());
        mGridView.setNumColumns(5);
        mGridView.setAdapter(new ColorAdapter());
        mGridView.setOnItemClickListener(this);

        builder.setView(mGridView);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Settings.System.putInt(getContext().getContentResolver(), "qs_accent_color",
                        mSelectedItem);
                mIndicator.setColorFilter(sColors[mSelectedItem], PorterDuff.Mode.MULTIPLY);
                Intent systemUiRestartIntent = new Intent("com.android.systemui.FORCE_RESTART");
                getContext().sendBroadcast(systemUiRestartIntent);
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mSelectedItem = position;
        ((ColorAdapter) mGridView.getAdapter()).notifyDataSetChanged();
    }

    private class ColorAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return sColors.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView iv = new ImageView(getContext());
            int drawableId = position == mSelectedItem ? R.drawable.color_picker_item_selected :
                    R.drawable.color_picker_item;
            iv.setImageDrawable(getContext().getResources().getDrawable(drawableId));
            iv.setColorFilter(sColors[position], PorterDuff.Mode.MULTIPLY);
            return iv;
        }
    }
}
