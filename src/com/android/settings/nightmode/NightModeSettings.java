package com.android.settings.nightmode;

import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ThemeChangeRequest;
import android.content.res.ThemeManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.provider.Settings;
import android.util.Log;

import java.util.ArrayList;

public class NightModeSettings extends SettingsPreferenceFragment implements
        Preference.OnPreferenceChangeListener {

    private static final String TAG = "NightModeSettings";

    private ArrayList<String> mInstalledThemesEntries;
    private ArrayList<String> mInstalledThemesPackages;
    private ArrayList<String> mInstalledThemesValues;

    private ListPreference mInterruptions;
    private ListPreference mThemeStyle;
    private ListPreference mThemeStatus;
    private ListPreference mThemeNav;
    private ListPreference mThemeIcons;
    private ListPreference mThemeWallpaper;
    private ListPreference mThemeLockscreen;

    private ContentResolver mContentResolver;
    private ThemeManager mThemeManager;
    private AudioManager mAudioManager;

    public NightModeSettings() {
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        mInstalledThemesEntries = new ArrayList<>();
        mInstalledThemesPackages = new ArrayList<>();
        mInstalledThemesValues = new ArrayList<>();
        mContentResolver = getContentResolver();
        addPreferencesFromResource(R.xml.nightmode_settings);
        mThemeStyle = (ListPreference) findPreference("nightmode_theme_style");
        mThemeStatus = (ListPreference) findPreference("nightmode_theme_status");
        mThemeNav = (ListPreference) findPreference("nightmode_theme_nav");
        mThemeIcons = (ListPreference) findPreference("nightmode_theme_icons");
        mThemeWallpaper = (ListPreference) findPreference("nightmode_theme_wallpaper");
        mThemeLockscreen = (ListPreference) findPreference("nightmode_theme_lockscreen");
        mInterruptions = (ListPreference) findPreference("nightmode_interruptions");

        LoadInstalledThemesAsyncTask task = new LoadInstalledThemesAsyncTask();
        task.execute();

        updateInterruptionsSummary();

        mThemeStyle.setOnPreferenceChangeListener(this);
        mThemeStatus.setOnPreferenceChangeListener(this);
        mThemeNav.setOnPreferenceChangeListener(this);
        mThemeIcons.setOnPreferenceChangeListener(this);
        mThemeWallpaper.setOnPreferenceChangeListener(this);
        mThemeLockscreen.setOnPreferenceChangeListener(this);
        mInterruptions.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newState) {
        if (preference == mThemeStyle) {
            int index = Integer.parseInt((String) newState);
            Settings.System.putString(mContentResolver, "nightmode_theme_style",
                    mInstalledThemesPackages.get(index + 1));
            updateThemeValuesAndSummaries();
            return true;
        }
        if (preference == mThemeStatus) {
            int index = Integer.parseInt((String) newState);
            Settings.System.putString(mContentResolver, "nightmode_theme_status",
                    mInstalledThemesPackages.get(index + 1));
            updateThemeValuesAndSummaries();
            return true;
        }
        if (preference == mThemeNav) {
            int index = Integer.parseInt((String) newState);
            Settings.System.putString(mContentResolver, "nightmode_theme_nav",
                    mInstalledThemesPackages.get(index + 1));
            updateThemeValuesAndSummaries();
            return true;
        }
        if (preference == mThemeIcons) {
            int index = Integer.parseInt((String) newState);
            Settings.System.putString(mContentResolver, "nightmode_theme_icons",
                    mInstalledThemesPackages.get(index + 1));
            updateThemeValuesAndSummaries();
            return true;
        }
        if (preference == mThemeWallpaper) {
            int index = Integer.parseInt((String) newState);
            Settings.System.putString(mContentResolver, "nightmode_theme_wallpaper",
                    mInstalledThemesPackages.get(index + 1));
            updateThemeValuesAndSummaries();
            return true;
        }
        if (preference == mThemeLockscreen) {
            int index = Integer.parseInt((String) newState);
            Settings.System.putString(mContentResolver, "nightmode_theme_lockscreen",
                    mInstalledThemesPackages.get(index + 1));
            updateThemeValuesAndSummaries();
            return true;
        }
        if (preference == mInterruptions) {
            Settings.System.putInt(mContentResolver, "nightmode_interruptions",
                    Integer.parseInt((String) newState));
            updateInterruptionsSummary();
            return true;
        }
        return false;
    }

    private void updateInterruptionsSummary() {
        int mode = Settings.System.getInt(mContentResolver, "nightmode_interruptions", -1);
        mInterruptions.setValue(Integer.toString(mode));
        mInterruptions.setSummary(mInterruptions.getEntry());
    }



    private void updateThemeValuesAndSummaries() {
        Log.d(TAG, "Updating theme values");
        String styleName = Settings.System.getString(mContentResolver, "nightmode_theme_style");
        String statusName = Settings.System.getString(mContentResolver,
                "nightmode_theme_status");
        String navName = Settings.System.getString(mContentResolver, "nightmode_theme_nav");
        String iconsName = Settings.System.getString(mContentResolver, "nightmode_theme_icons");
        String wallpaperName = Settings.System.getString(mContentResolver,
                "nightmode_theme_wallpaper");
        String lockscreenName = Settings.System.getString(mContentResolver,
                "nightmode_theme_lockscreen");
        Log.d(TAG, "New values: " + styleName + statusName + navName + iconsName);
        mThemeStyle.setValue(mInstalledThemesValues.get(mInstalledThemesPackages
                .indexOf(styleName)));
        mThemeStyle.setSummary(mThemeStyle.getEntry());
        mThemeStatus.setValue(mInstalledThemesValues.get(mInstalledThemesPackages
                .indexOf(statusName)));
        mThemeStatus.setSummary(mThemeStatus.getEntry());
        mThemeNav.setValue(mInstalledThemesValues.get(mInstalledThemesPackages
                .indexOf(navName)));
        mThemeNav.setSummary(mThemeNav.getEntry());
        mThemeIcons.setValue(mInstalledThemesValues.get(mInstalledThemesPackages
                .indexOf(iconsName)));
        mThemeIcons.setSummary(mThemeIcons.getEntry());
        mThemeWallpaper.setValue(mInstalledThemesValues.get(mInstalledThemesPackages
                .indexOf(wallpaperName)));
        mThemeWallpaper.setSummary(mThemeWallpaper.getEntry());
        mThemeLockscreen.setValue(mInstalledThemesValues.get(mInstalledThemesPackages
                .indexOf(lockscreenName)));
        mThemeLockscreen.setSummary(mThemeLockscreen.getEntry());
    }

    private class LoadInstalledThemesAsyncTask extends AsyncTask<String, Integer, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            mInstalledThemesEntries.add(getResources().getString(R.string.nightmode_theme_do_not_change));
            mInstalledThemesPackages.add(null);
            mInstalledThemesValues.add("-1");
            mInstalledThemesEntries.add(getResources().getString(R.string.nightmode_theme_system));
            mInstalledThemesPackages.add("system");
            mInstalledThemesValues.add("0");
            PackageManager pm = getPackageManager();
            int number = 0;
            for (PackageInfo pi : pm.getInstalledPackages(0)) {
                if (pi.isThemeApk) {
                    number++;
                    mInstalledThemesEntries.add(pi.themeInfo.name);
                    mInstalledThemesPackages.add(pi.packageName);
                    mInstalledThemesValues.add(Integer.toString(number));
                }
            }
            return number;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            mThemeStyle.setEntries(mInstalledThemesEntries
                    .toArray(new CharSequence[mInstalledThemesEntries.size()]));
            mThemeStyle.setEntryValues(mInstalledThemesValues
                    .toArray(new CharSequence[mInstalledThemesValues.size()]));
            mThemeStatus.setEntries(mInstalledThemesEntries
                    .toArray(new CharSequence[mInstalledThemesEntries.size()]));
            mThemeStatus.setEntryValues(mInstalledThemesValues
                    .toArray(new CharSequence[mInstalledThemesValues.size()]));
            mThemeNav.setEntries(mInstalledThemesEntries
                    .toArray(new CharSequence[mInstalledThemesEntries.size()]));
            mThemeNav.setEntryValues(mInstalledThemesValues
                    .toArray(new CharSequence[mInstalledThemesValues.size()]));
            mThemeIcons.setEntries(mInstalledThemesEntries
                    .toArray(new CharSequence[mInstalledThemesEntries.size()]));
            mThemeIcons.setEntryValues(mInstalledThemesValues
                    .toArray(new CharSequence[mInstalledThemesValues.size()]));
            mThemeWallpaper.setEntries(mInstalledThemesEntries
                    .toArray(new CharSequence[mInstalledThemesEntries.size()]));
            mThemeWallpaper.setEntryValues(mInstalledThemesValues
                    .toArray(new CharSequence[mInstalledThemesValues.size()]));
            mThemeLockscreen.setEntries(mInstalledThemesEntries
                    .toArray(new CharSequence[mInstalledThemesEntries.size()]));
            mThemeLockscreen.setEntryValues(mInstalledThemesValues
                    .toArray(new CharSequence[mInstalledThemesValues.size()]));
            updateThemeValuesAndSummaries();
        }
    }
}